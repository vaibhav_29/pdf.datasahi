function getFile() {
    document.getElementById("upfile").click();
  }
  
  function sub(obj) {
    var file = obj.value;
    var fileName = file.split("\\");
    document.getElementById("yourBtn").innerHTML = fileName[fileName.length - 1];
    document.myForm.submit();
    event.preventDefault();
  }


  function storeContactUsValues() { 

    var email = document.getElementById('Email').value
    var name = document.getElementById('Name').value
    var phone = document.getElementById('Phone').value
    var message = document.getElementById('Message').value



    // if(vaibhav){
        const scriptUrl = 'https://script.google.com/macros/s/AKfycbxNB0bUlAG4orSN2fhx9iCZ7O7xLk_ox4qV3LwCa4wlrimjkBc/exec'
        var ele = document.getElementsByTagName('input'); 
        var Message = document.getElementById('Message');
        var formData = new FormData();
        formData.append('Name', name)
        formData.append('Email', email)
        formData.append('Phone', phone)
        formData.append('Message', message)
        fetch(scriptUrl,{ method: 'POST', body: formData})
    .then(response => {if(response.status === 200){
        swal("Thank You!", "", "success")
        .then((value) => {
            location.reload()
        });
    }} )
    .catch(error => console.error('Error!', error.message))

} 


//pop up name dymamically
function getData(event, data,img){

    document.getElementById("dbtext").innerText=data+".pdf";
    document.getElementById("thumbnail").src= img;
    document.getElementById('extract').style.display= "inline-block";
    document.getElementById('btntext').textContent= "";
    
};


function submitform(){

var name1=true;
var email1=true;
var phone1=true;
var email = document.getElementById('Email').value
var name = document.getElementById('Name').value
var phone = document.getElementById('Phone').value
var message = document.getElementById('Message').value

if(name.length === 0){
    document.getElementById("nameValidation").innerHTML = 'please enter the name';
    name1=false;
}else{
    document.getElementById("nameValidation").innerHTML = '';
    name1=true;
}

if(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
    document.getElementById("emailValidation").innerHTML = '';
    email1 = true;
    
}else{
        document.getElementById("emailValidation").innerHTML = 'invalid email';
        email1 = false;
}

if(phone.length != 10){
    document.getElementById("phoneValidation").innerHTML = 'Enter 10 digit number';
    phone1=false
}else{
    document.getElementById("phoneValidation").innerHTML = '';
    phone1=true
}


if(name1 === true && email1 === true && phone1 === true){
    storeContactUsValues();
}
else{
    document.getElementById("formValidation").innerHTML = 'Please fill required fields';
}

}

function extract(data) {
 console.log(data)
  var hello= document.getElementById("dbtext").innerText
  console.log(hello)
  
  var request = {};
  var request1 = {};
  

  if(hello==="rbl_creditCard.pdf"){
    request.parserId="rbl.funplus.standard",
    request.pdfFile="src/main/mnt/suki/temp/rbl.pdf",
    request1.rulesFile="src/main/mnt/suki/parser/rbl_funplus_rules.json"
  }
  if(hello==="Invoice.pdf"){
    request.parserId="invoice.rules",
    request.pdfFile="src/main/mnt/suki/temp/Invoice.pdf",
    request1.rulesFile="src/main/mnt/suki/parser/parser_rules_invoice.json"
  }
  if(hello==="passport_payment.pdf"){
      request.parserId = "passport_payment.rules",
      request.pdfFile = "src/main/mnt/suki/temp/PaymentReceipt.pdf",
      request1.rulesFile="src/main/mnt/suki/parser/parser_rules_passport_payment.json"
  }
  if(hello==="payslip.pdf"){
    request.parserId="payslip.rules",
    request.pdfFile="src/main/mnt/suki/temp/payslip.pdf",
    request1.rulesFile="src/main/mnt/suki/parser/parser_rules_payslip.json"
  }
  
  
    // console.log(request);
    const requestedData = {
      method: "Post",
      body: JSON.stringify(request),
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*"
      }
    };
    fetch("https://suki.datasahi.com/pdftojson/extract", requestedData)
      .then(response => response.json())
      .then(data => {
        // console.log(JSON.stringify(data, null, 4));
        document.getElementById('response').innerHTML= (JSON.stringify(data, null, 4));
        document.getElementById('btntext').textContent= "DATA EXTRACTED";
        document.getElementById('extract').style.display= "none";
      });



      console.log(request1);
      const requestedData1 = {
        method: "Post",
        body: JSON.stringify(request1),
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        }
      };
      fetch("https://suki.datasahi.com/pdftojson/rules", requestedData1)
        .then(response => response.json())
        .then(data => {
          console.log(JSON.stringify(data, null, 4));
          document.getElementById('Rulesresponse').innerHTML= (JSON.stringify(data, null, 4));
          
        });
  }

  function Close(){
    console.log("::::::::::::::::")
    document.getElementById('response').innerHTML= " ";
    document.getElementById('Rulesresponse').innerHTML= " ";
    // document.getElementById('btntext').innerHTML= <button type="button" class="btn btn-success mb-4" id="extract" onclick="extract(event)">Extract</button>;
  }


  function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
  }